package main

import (
	"fmt"
	"got-a-break/errors/custom"
)

func takeFuel(fuelType string) error {
	if fuelType == "diesel" {
		return custom.GazError{}
	}

	return nil
}

func goToGazStationForDiesel() error {
	err := takeFuel("diesel")
	if err != nil {
		return fmt.Errorf("couldn't go to gaz station: %w", err)
	}

	return nil
}

func main() {
	err := goToGazStationForDiesel()
	if err != nil {
		fmt.Println(err)
	}
}
