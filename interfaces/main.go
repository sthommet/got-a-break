package main

import "fmt"

func main() {

	var a int = 1
	var b int = 1

	setViaInterface(a)
	setViaPtr(&b)

	fmt.Println(a)
	fmt.Println(b)

	//fmt.Stringer()
}

func setViaInterface(val interface{}) {
	val = 2
}

func setViaPtr(val *int) {
	*val = 2
}
