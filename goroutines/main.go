package main

import (
	"fmt"
	"time"
)

func main() {
	// create channels
	serverResponse := make(chan int)
	emergencyMsg := make(chan string)

	// launch go routines
	go readResponses(serverResponse)
	go readEmergency(emergencyMsg)

	// select and handle responses (in a sequential style)
	for {
		select {
		case response := <-serverResponse:
			fmt.Println("Channel response:", response)

		case emergency := <-emergencyMsg:
			fmt.Println("Channel emergency:", emergency)
		}
	}
}

func readResponses(response chan int) {
	for {
		time.Sleep(2 * time.Second)

		response <- 15
	}
}

func readEmergency(emergency chan string) {
	//something wrong happened...

	emergency <- "Titanic is sinking !"
}
