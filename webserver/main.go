package main

import (
	"fmt"
	"net/http"
	//"github.com/hako/durafmt"
)

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "world !\n")

	//timeduration := (354 * time.Hour) + (22 * time.Minute) + (3 * time.Second)
	//duration := durafmt.Parse(timeduration).String()
	//fmt.Fprintf(w, duration) // 2 weeks 18 hours 22 minutes 3 seconds*/
}

func headers(w http.ResponseWriter, req *http.Request) {
	for name, headers := range req.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n", name, h)
		}
	}
}

func main() {
	http.HandleFunc("/hello", hello)
	http.HandleFunc("/headers", headers)

	err := http.ListenAndServe(":8090", nil)
	if err != nil {
		fmt.Println(err.Error())
	}
}
