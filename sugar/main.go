package main

import "fmt"

func main() {

	tripList := []string{"Paris, London, NY, Tourcoing"}

	for i := 0; i < len(tripList); i++ {
		fmt.Println(i, tripList[i])
	}

	books := map[string]int{
		"maths":     5,
		"biology":   9,
		"chemistry": 6,
		"physics":   3,
	}

	for key, val := range books {
		fmt.Println(key, val)
	}
}
