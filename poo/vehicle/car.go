package vehicle

import (
	"fmt"
	"got-a-break/poo/client"
)

type Klaxon struct {
	song string
}

func (k *Klaxon) BipBip() {
	fmt.Println(k.song)
}

type Car struct {
	totalPassengers int
	maxSpeed        int
	hasAirCond      bool
	klaxon          Klaxon
}

func (c *Car) LetPassengerIn(client client.Client) {
	c.totalPassengers = c.totalPassengers + client.NbPassenger

	c.klaxon.BipBip()
}

func CreateCar() *Car {
	return &Car{
		maxSpeed:   130,
		hasAirCond: true,
		klaxon: Klaxon{
			song: "Bip bip !",
		},
	}
}
