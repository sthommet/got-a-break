package vehicle

import "got-a-break/poo/client"

type Vehicle interface {
	LetPassengerIn(client client.Client)
}
