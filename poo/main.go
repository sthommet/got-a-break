package poo

import (
	"fmt"
	"got-a-break/poo/client"
	. "got-a-break/poo/vehicle"
)

func main() {
	var firstVehicle Vehicle = CreateCar()

	firstVehicle.LetPassengerIn(client.Client{NbPassenger: 2})

	fmt.Println(firstVehicle)
}
